# Pkging (Experimental)
The go-to package installer 

## Usage
```bash
# Because this is early stage I haven't build a binary yet, so you'll have to install the dependencies using : 
yarn install

# Get an overview of the available commands :
node index.js help

# Install packages from a profile.json file : (You don't have to use root, you'll get a prompt for your password in the terminal)
node index.js install ./profile.json
```

## Support
Currently supports : 
- Flatpak
- Dnf (yum redirects to Dnf)
- Apt
- Zypper (experimental)
- Pacman (Experimental)
- Snap (Experimental)

## Example profile.json
```json
{
    "metadata": {
        "name": "Example application profile",
        "description": "Description of your example application profile",
        "pkging_version": "0.0.1"
    },
    "package_groups": [
        {
            "name": "Example package group",
            "description": "Example description",
            "packages": [{...}, {...}]
        }
    ],
    "packages" : [
        {
            "type": "flatpak",
            "id": "https://dl.flathub.org/repo/appstream/com.example.exampleApp.flatpakref",
            "name": "Example App",
            "comment": "Add some comments here"
        },
        {
            "type": "dnf",
            "id": "example-dnf-package"
        }
    ]
}
```
