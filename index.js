const { Command } = require('commander');
const config = require('./src/config/config');
const path = require('path');

if(config.isSetupRequired()) {
    console.log("Looks like this is the first time you're using Pkging. We'll have to do some configuration first.")
    config.setup(); 
    return
}

config.configure();
const pkging = new Command();

pkging
  .name('pkging')
  .description('Manage all your packages.')
  .version(process.env.VERSION);

pkging.command('install')
  .description('Install applications found in your profile')
  .argument('<string>', 'Path to application profile')
  .action((profileFile, options) => require("./src/install/install")(path.resolve(__dirname, path.normalize(profileFile)), options));
pkging.command('config')
    .description('(Re-)Configure Pkging')
    .action(config.setup)

pkging.parse();