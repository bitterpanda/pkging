const fs = require("fs");
const os = require("os");
const inquirer = require('inquirer');

const CONFIG_PATH = os.homedir() + "/.local/share/pkging";

const isSetupRequired = () => {
    return !fs.existsSync(CONFIG_PATH + "/config.json");
};

const setup = () => {
    inquirer
        .prompt([
            {name: 'packageManagers', message: 'Which package managers would you like to enable?', type: 'checkbox', choices: ["Flatpak", "Dnf", "Pacman", "Zypper", "Yum", "Apt"]}
        ])
        .then(answers => {
            let configuration = {
                enabledPackageManagers: answers.packageManagers.map(x => x.toLocaleLowerCase())
            }
            fs.mkdirSync(CONFIG_PATH, {recursive:true})
            fs.writeFileSync(CONFIG_PATH + "/config.json", JSON.stringify(configuration), 'utf-8');
        })
        .catch(err => {
            console.log("Something went wrong : ", err);
        })
}
const configure = () => {
    const configuration = JSON.parse(fs.readFileSync(CONFIG_PATH + "/config.json"));
    process.env.enabledPackageManagers = configuration.enabledPackageManagers;
}
module.exports = {setup, isSetupRequired, configure};