const printMetadata = profile => {
    console.log("Profile name: \033[1;4m" + profile.metadata.name + "\033[0m");
    console.log(`Profile description: ${profile.metadata.description}`)
};

module.exports = printMetadata;