const { spawnSync } = require('child_process');

const rewrite = (text) => {
    process.stdout.clearLine(0);
    process.stdout.cursorTo(0);
    console.log(text);
}

const errorHandler = (err, packageName) => {
    rewrite("Installation of " + packageName + " \33[31;1mfailed")
    console.error(err);
    console.log("\33[0m");
}

const packageAlreadyInstalled = (data, string, packageManager, packageName) => {
    if(data.includes(string)) {
        rewrite(`[${packageManager}] ` + "Package \33[1m" + packageName + "\33[0m already installed");
        return true;
    }
}

const stderrHandler = (data, alreadyInstalledErrorString, packageManager,packageName, output) => {
    data = data.toString();

    if(packageAlreadyInstalled(data, alreadyInstalledErrorString, packageManager, packageName)) {return 2}
    if(data) {
        errorHandler(data, packageName);
        return 2
    }
};
const stdoutHandler = (data, alreadyInstalledErrorString, packageManager, packageName, output) => {
    data = data.toString();
    if(packageAlreadyInstalled(data, alreadyInstalledErrorString, packageManager, packageName)) { return 2 }
}
const closeHandler = (code, packageManager, packageName, output) => {
    if(code !== 0 && output == 0)  rewrite(`[${packageManager}] ` + "Failed to install \33[1;31m" + packageName + "\33[0m");
    else if(output == 0) rewrite(`[${packageManager}] ` + "Succesfully installed \33[1;32m" + packageName + "\33[0m");
}

const installer = (commandlineArgs, packageName, packageManager, alreadyInstalledErrorString) => {
    process.stdout.write(`[${packageManager}] ` + "Attempting to install \33[1m" + packageName + "\33[0m...");
    const cli = spawnSync(commandlineArgs.split(" ")[0], commandlineArgs.split(" ").slice(1));
    let output = 
        stderrHandler(cli.stderr, alreadyInstalledErrorString,packageManager,packageName) == 2 ||
        stdoutHandler(cli.stdout, alreadyInstalledErrorString,packageManager,packageName) == 2;
    
    closeHandler(cli.status, packageManager, packageName, output);

    return cli;
};

module.exports = installer;