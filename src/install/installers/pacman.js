const installer = require('./generalInstaller');

const pacmanInstaller = packages => {
    console.log("\33[1;33mExperimental Pacman support\33[0m");
    for (let i = 0; i < packages.length; i++) {
        const package = packages[i];
        installer(`sudo pacman -Sy ${package.id}`, (package.name || package.id), "Pacman", "already installed")
    }
};

module.exports = pacmanInstaller;