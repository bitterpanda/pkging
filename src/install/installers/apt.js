const installer = require('./generalInstaller');

const aptInstaller = packages => {
    for (let i = 0; i < packages.length; i++) {
        const package = packages[i];
        installer(`sudo apt install ${package.id} -y`, (package.name || package.id), "Apt", "already installed")
    }
};

module.exports = aptInstaller;