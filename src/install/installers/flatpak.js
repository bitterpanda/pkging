const installer = require('./generalInstaller');

const flatpakInstaller = packages => {
    for (let i = 0; i < packages.length; i++) {
        const package = packages[i];
        installer(`flatpak install ${package.id} -y`, (package.name || package.id), "Flatpak", "already installed")
    }
};

module.exports = flatpakInstaller;