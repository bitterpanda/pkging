const installer = require('./generalInstaller');

const zypperInstaller = packages => {
    console.log("\33[1;33mExperimental Zypper support\33[0m");
    for (let i = 0; i < packages.length; i++) {
        const package = packages[i];
        installer(`sudo zypper install ${package.id} -y`, (package.name || package.id), "Zypper", "already installed")
    }
};

module.exports = zypperInstaller;