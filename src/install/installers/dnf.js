const installer = require('./generalInstaller');

const dnfInstaller = packages => {
    for (let i = 0; i < packages.length; i++) {
        const package = packages[i];
        installer(`sudo dnf install ${package.id} -y`, (package.name || package.id), "DNF", "already installed")
    }
};

module.exports = dnfInstaller;