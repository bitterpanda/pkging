const installer = require('./generalInstaller');

const snapInstaller = packages => {
    console.log("\33[1;33mExperimental Snap support\33[0m");
    for (let i = 0; i < packages.length; i++) {
        const package = packages[i];
        installer(`sudo snap install -y ${package.id}`, (package.name || package.id), "Snap", "already installed")
    }
};

module.exports = snapInstaller;