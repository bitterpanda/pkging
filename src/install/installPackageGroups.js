const inquirer = require('inquirer');
const installPackages = require("./installPackages");

const installEnabledPackages = (groups, enabledGroups) => {
    for (let i = 0; i < groups.length; i++) {
        const group = groups[i];
        if(!enabledGroups.includes(group.name)) { continue }
        installPackages(group.packages);
    }
}

const installPackageGroups = async groups => {
    const group_names = groups.map(x => x.name + (x.description ? (" - " + x.description) : ""));
    await inquirer
        .prompt([
            {name: 'packageGroups', message: 'Which package groups would you like to install?', type: 'checkbox', choices: group_names}
        ])
        .then(answers => {
            installEnabledPackages(groups, answers.packageGroups.map(x => x.split(" - ")[0]))
        })
        .catch(err => {
            console.log("Something went wrong : ", err);
        })
};

module.exports = installPackageGroups; 