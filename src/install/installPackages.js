const splitPackages = require("./splitPackages");

const installPackages = packages => {
    const packagesDividedIntoManagers = splitPackages(packages);
    for (const pkgManager in packagesDividedIntoManagers) {
        //console.log("\33[1mInstalling " + pkgManager + " packages\33[0m");
        let pkgManagerPackages = packagesDividedIntoManagers[pkgManager];

        if(!process.env.enabledPackageManagers.includes(pkgManager.toLocaleLowerCase())) continue;
        require("./installers/" + pkgManager)(pkgManagerPackages);
    }
};

module.exports = installPackages;