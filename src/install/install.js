// Install command

const install = async (profileFile, options) => {
    const profile = require(profileFile);
    require("./../info/printMetadata")(profile);

    console.log("\nInstalling packages...");

    await require("./installPackageGroups")(profile.package_groups);
    require("./installPackages")(profile.packages);

}

module.exports = install;