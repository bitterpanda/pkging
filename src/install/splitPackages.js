const splitPackages = packages => {
    const splitPackages = {};
    for (let i = 0; i < packages.length; i++) {
        const package = packages[i];
        if(!splitPackages[package.type]) splitPackages[package.type] = [];
        splitPackages[package.type].push(package);
    }
    
    return splitPackages;
}

module.exports = splitPackages;